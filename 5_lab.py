import matplotlib.pyplot as plt
import numpy as np
import urllib.request
import re

#file_url = "https://www.pythonlearn.com/code/mbox.txt"
#data = urllib.request.urlopen(file_url).read() #почему-то не работает - код 403

file_name = "mbox.txt" #название файла
f = open(file_name, "r") #открываем файл
data = f.read() #считываем из него все данные

messages = data.split('\n\n\n\n') #сообщение разделены четырьмя переходами строки. разделяем файл на список сообщений
#формирование необходимых данных о сообщениях: имя отправителя, X-DSPAM-Confidence
messages_data = []
for m in messages:
    #определение автора сообщения
    if re.search('Author:*', m): #если данные об авторе указаны
        author_data = re.search('Author: .*', m).group(0) #получаем строку с указанием автора письма
        author = author_data.split(' ')[1] #отделяем от нее e-mail отправителя
        #определеяем показатель спама сообщения
        if re.search('X-DSPAM-Confidence:.*', m):  # если данные о спаме указаные
            conf_data = re.search('X-DSPAM-Confidence:.*', m).group(0)  # получаем строку с коэффициентом
            conf = float(conf_data.split(' ')[1])  # отделяем от нее значнеие X-DSPAM
            messages_data.append((author, conf)) #заносим в список отправителя и коэффициент X-DSPAM-Confidence

#определение среднего X-DSPAM-Confidence
sum = 0.0
for md in messages_data:
    sum += md[1]
avr = sum / len(messages_data)
print('Среднее значение X-DSPAM-Confidence:', avr)

#опеределение, кого стоит заблокировать за спам
print('Предположительно следует заблокировать за спам (X-DSPAM-Confidence > 0.95): ')
for md in messages_data:
    if (md[1] > 0.95):
        print(md[0], '(', md[1], ')')

#формирование данных для графика: авторы и кол-во писем
authors = []
counts = []
for m in messages_data: #просмотр всех сообщений
    if m[0] not in authors: #если данных об авторе еще нет в списке
        #считаем все его письма
        a = m[0]
        count = 0
        #просмотр писем и подсчет писем авторма
        for m2 in messages_data:
            if m2[0] == a:
                count += 1
        authors.append(a) #добавляем атвора в список
        counts.append(count) #количество его писем
#построение гистограмы
graph4 = plt.figure(1) #новое окно графика
plt.title("Количество отправленных писем") #название
#отображаем 10 первых отправителей
y_pos = np.arange(len(authors[:10])) #последовательность номеров столбцов
plt.bar(y_pos, counts[:10]) #создаем столбцы
plt.xticks(y_pos, authors[:10]) #подписи для столбцов
plt.show()
