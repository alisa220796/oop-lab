import random
import matplotlib.pyplot as plt

n = int(input("Введите количество исходных данных (мин. 25): "))
data = [0] * n #пустой список с исходными данными

print('\nВвод данных\n1. Ввести вручную')
print('2. Сгенерировать случайные')
input_type = (int(input('Выберите вариант заполнения исходных данных: ')))
#ввод данных
if (input_type == 1):
    #вручную
    for i in range(n):
        data[i] = float(input('Введите значение №%d: ' % (i)))
else:
    #случайными числами
    for i in range(n):
        data[i] = random.randint(1, 100)

w = int(input('Введите значение окна (длина интервала сглаживания): '))

#вычисление скользящей средней
m = n - w #кол-во значений, для которых может быть вычислена SMA
sma = [0] * m
for i in range(m):
    #суммируем значения, входящие в интервал для текущих данных
    sum = 0
    for j in range(i, i+w):
        sum += data[j]
    sma[i] = sum / w

#строим графики
x = list(range(n))
fig, ax = plt.subplots()
ax.plot(x, data, label="F(x)") #исходные данные
ax.plot(x[w:], sma, label="SMA") #скользящее среднее
ax.legend()

plt.show()
