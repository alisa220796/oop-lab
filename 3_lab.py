# -*- coding: utf-8 -*-

import argparse, sys

parser = argparse.ArgumentParser() #будем разбирать полученные через командную строку аргументы
parser.add_argument('--poly') #получаем значение аргумента "poly" - строка с коэффицентами
args = parser.parse_args()

poly_coeffs = args.poly #получаем строку с значениями коэфиицинтов
coeffs = [int(c) for c in poly_coeffs.split(',')] #преобразуем в список целых чисел, разделяя через запятую

#вычисляем сумму рядя
s = 0.0 #сумма
for c in coeffs:
    s += (1.0 / c * 3) #прибавляем к сумме слагаемое

print(s)
